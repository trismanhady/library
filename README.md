
## Cara Instalasi
Cloning  repo:
```shell
git clone https://gitlab.com/trismanhady/library.git
```

Setelah berhasil cloning, arahkan shell ke folder hasil clonning

Install composer packages:
```shell
composer update
```

Copy dan rename .env.example to .env.
Buat database di PHPmyAdmin dengan nama db_library

Masuk ke shell ketikkan
```shell
php artisan key:generate
```

Lakukan migrate dan update isi database user:
```shell
php artisan migrate
```
```shell
php artisan db:seed
```

ketikkan di browser <div style="display: inline">http://localhost:8000/</div>

#### Demo Admin Login
*  Email: admin@example.com
*  Password: 1234

#### Demo Pegawai Login
*  Email: pegawai@example.com
*  Password: 1234

#### Demo Pengguna Login
*  Email: pengguna1@example.com
*  Password: 1234

