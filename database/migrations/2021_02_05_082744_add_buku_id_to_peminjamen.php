<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBukuIdToPeminjamen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('peminjamen', function (Blueprint $table) {
            $table->unsignedBigInteger('buku_id');
            $table->foreign('buku_id')->references('id')->on('buku');

            $table->unsignedBigInteger('anggota_id');
            $table->foreign('anggota_id')->references('id')->on('anggota');

            $table->unsignedBigInteger('pengembalian_id');
            $table->foreign('pengembalian_id')->references('id')->on('pengembalians');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('peminjamen', function (Blueprint $table) {
            $table->dropForeign(['pengembalian_id']);
            $table->dropColumn(['pengembalian_id']);

            $table->dropForeign(['anggota_id']);
            $table->dropColumn(['anggota_id']);

            $table->dropForeign(['buku_id']);
            $table->dropColumn(['buku_id']);
        });
    }
}
